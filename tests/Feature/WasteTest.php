<?php

namespace Tests\Feature;

use Database\Factories\UserFactory;
use Database\Factories\WasteFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class WasteTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_the_application_can_list_wastes(): void
    {
        $user = UserFactory::new()->create();

        $response = $this->actingAs($user)
            ->getJson('/api/');


       $response->assertStatus(200);
    }

    public function test_the_application_can_show_waste(): void
    {
        $user = UserFactory::new()->create();
        $waste = WasteFactory::new()->create();


        $response = $this->actingAs($user)
            ->getJson("/api/waste/$waste->id");


        $response->assertStatus(200);
    }

    public function test_the_application_can_update_waste(): void
    {
        $user = UserFactory::new()->create();
        $waste = WasteFactory::new()->create();


        $response = $this->actingAs($user)
            ->putJson("/api/waste/$waste->id", [
                'name' => 'Garrafa'
            ]);


        $response->assertStatus(200);
    }


    public function test_the_application_can_delete_waste(): void
    {
        $user = UserFactory::new()->create();
        $waste = WasteFactory::new()->create();


        $response = $this->actingAs($user)
            ->deleteJson("/api/waste/$waste->id");


        $response->assertStatus(200);
    }
}
