<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class WasteFactory extends Factory
{
    /**
     * @return array|mixed[]
     */
    public function definition()
    {
        return [
            'category' => fake()->name(),
            'class'=> fake()->name(),
            'name' => fake()->name(),
            'type'=> fake()->name(),
            'technology'=> fake()->name(),
            'unit'=> fake()->name(),
            'weight' => fake()->randomFloat()
        ];
    }
}
