## API 

API RESTful que receberá uma planilha de residuos, processada em background (queue).
Endpoint que informe se a planilha for processada com sucesso ou
não.
Possível visualizar, atualizar e apagar os resíduos (só é possível criar
novos produtos via planilha).

## Requisitos técnicos
Laravel.
Docker
Testes automatizados 
Git/Gitlab.
REST.
JSON
