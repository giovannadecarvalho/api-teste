<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Waste extends Model
{
    protected $fillable = [
        'category',
        'class',
        'name',
        'type',
        'technology',
        'unit',
        'weight',
    ];

}
