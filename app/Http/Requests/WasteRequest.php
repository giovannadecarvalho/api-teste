<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WasteRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return $this->method() == 'POST'
            ? [
                'spreadsheet'   => 'required'
            ]
            :
            [
                'name'          => 'nullable',
                'type'          => 'nullable',
                'category'      => 'nullable',
                'technology'    => 'nullable',
                'class'         => 'nullable',
                'unit'          => 'nullable',
                'weight'        => 'nullable',
            ];
    }
}
