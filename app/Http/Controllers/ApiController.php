<?php

namespace App\Http\Controllers;

use App\Http\Requests\WasteRequest;
use App\Jobs\ProcessSpreadsheet;
use App\Models\Waste;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Rap2hpoutre\FastExcel\FastExcel;


class ApiController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function processSpreadsheet(Request $request)
    {
        try {
            $file = $request->file('spreadsheet');

            if (empty($file)) {
                return response()->json("Planilha não enviada", Response::HTTP_BAD_REQUEST);
            }

            $file_path  = $file->store('uploadsXlsx', ['disk' => 'public']);
            $path       = Storage::disk('public')->path($file_path);
            $wastes     = (new FastExcel)->importSheets($path);
            $dataWastes = [];

            if (empty($wastes)) {
                return response()->json("Dados não encontrados", Response::HTTP_NOT_FOUND);
            }

            foreach ($wastes[0] as $waste) {

                $dataWastes[] = [
                    'name'          => $waste['Nome Comum do Resíduo'],
                    'type'          => $waste['Tipo de Resíduo'],
                    'category'      => $waste['Categoria'],
                    'technology'    => $waste['Tecnologia de Tratamento'],
                    'class'         => $waste['Classe'],
                    'unit'          => $waste['Unidade de Medida'],
                    'weight'        => $waste['Peso'],
                ];
            }

            ProcessSpreadsheet::dispatch($dataWastes);

            return response()->json('Dados processados com sucesso!', Response::HTTP_OK);
        } catch (\Throwable $ex) {
            report($ex);
            return response()->json('Erro ao processar os dados da planilha.', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        try {
            $waste = Waste::query()
                ->where('id', $id)
                ->first();

            if (empty($waste)) {
                return response()->json("Resíduo não encontrado", Response::HTTP_NOT_FOUND);
            }

            return response()->json($waste, Response::HTTP_OK);
        } catch (\Throwable $ex) {
            report($ex);
            return response()->json('Erro ao atualizar os dados.', Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * @param $id
     * @param WasteRequest $request
     * @return JsonResponse
     */
    public function update($id, WasteRequest $request)
    {
        try {
            $data   = $request->validated();

            Waste::query()
                ->where('id', $id)
                ->update($data);

            return response()->json('Resíduo atualizado com sucesso!', Response::HTTP_OK);


        } catch (\Throwable $ex) {
            report($ex);
            return response()->json('Erro ao atualizar os dados.', Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * @return JsonResponse
     */
    public function getAllWastes()
    {
        try {
            $wastes = Waste::query()->get();

            return response()->json($wastes, Response::HTTP_OK);

        } catch (\Throwable $ex) {
            report($ex);
            return response()->json('Erro ao pesquisar.', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        try {
            $waste =  Waste::query()->where('id', $id)->first();

            if (empty($waste)) {
                return response()->json('Não encontrado', Response::HTTP_NOT_FOUND);
            }

            $waste->delete();

            return response()->json('Deletada com sucesso', Response::HTTP_OK);

        } catch (\Throwable $ex) {
            report($ex);
            return response()->json('Erro ao deletar.', Response::HTTP_BAD_REQUEST);
        }

    }

}
